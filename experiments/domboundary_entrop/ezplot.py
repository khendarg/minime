#!/usr/bin/env python3
from minime import entropystats, hydrostats
import matplotlib.pyplot as plt
import argparse
from Bio import SeqIO

if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument('infile', help='A FASTA of some sort')
	parser.add_argument('--torsion-window', default=9, help='Window for smoothing conformational entropy')
	parser.add_argument('--shannon-window', default=9, help='Window for smoothing Shannon entropy')
	parser.add_argument('--shannon-subwindow', default=19, help='Window for collecting Shannon entropy')
	parser.add_argument('--hydro-window', default=19, help='Window for smoothing hydropathy')
	args = parser.parse_args()

	with open(args.infile) as f:
		for record in SeqIO.parse(f, 'fasta'):
			hydro = hydrostats.Hydropathy(record.seq, window=args.hydro_window)
			shannon = entropystats.Shannon(record.seq, window=args.shannon_window, subwindow=args.shannon_subwindow)
			torsion = entropystats.Conformational(record.seq, window=args.torsion_window)
			fig = plt.figure()
			ax = fig.add_subplot(111)
			ax.axhline(0, c='k')
			ax.axvline(0, c='k')
			ax.plot(hydro.hydropathy)
			ax.plot(shannon.entropy)
			ax.plot(torsion.entropy)
			ax.set_title('Hydropathy, Shannon entropy, and conformational entropy')
			plt.show()
			
