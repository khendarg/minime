#!/usr/bin/env python3

import matplotlib.pyplot as plt
import numpy as np
from minime  import hydrostats, entropystats

seq = 'ACDEFGHIKLMNPQRSTVWY'
hydro = hydrostats.Hydropathy(seq, window=1)
y_h = hydro.get_hydropathy()

entrop = entropystats.Conformational(seq, window=1)
y_e = entrop.get_entropy()

fig = plt.figure()
ax = fig.add_subplot(111)
ax.plot(y_h, y_e, marker='.', lw=0)
ax.set_aspect(1)

print(np.corrcoef(y_h, y_e))
plt.show()
