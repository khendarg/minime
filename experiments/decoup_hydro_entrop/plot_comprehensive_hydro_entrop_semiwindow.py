#!/usr/bin/env python3

import matplotlib.pyplot as plt
import numpy as np
from minime  import hydrostats, entropystats
from Bio import SeqIO

window = 19
with open('tcdb.faa') as f:
	records = SeqIO.parse(f, 'fasta')
	hydro = hydrostats.Hydropathy('', window=window)
	entrop = entropystats.Conformational('', window=3)

	allhydro = np.array([])
	allentrop = np.array([])
	allletters = ''
	for fasta in records:
		if '.1.1.1-' not in fasta.name: continue
		if fasta.name.startswith('1.B'): continue

		hydro.seq = fasta.seq
		entrop.seq = fasta.seq
		allletters += fasta.seq
		allhydro = np.hstack([allhydro, hydro.get_hydropathy()])
		allentrop = np.hstack([allentrop, entrop.get_entropy()])

fig = plt.figure()
ax = fig.add_subplot(111)
#ax.plot(allhydro, allentrop, marker='.', lw=0, markersize=0.1)
#ax.set_aspect(1)

realhydro, realentrop = [], []
realletters = ''
for h, e, r in zip(allhydro, allentrop, allletters):
	if np.isnan(h) or np.isnan(e): continue
	else: 
		realhydro.append(h)
		realentrop.append(e)
		realletters += r

cov = np.cov(realhydro, realentrop)
print(cov)
va, ve = np.linalg.eig(cov)
print(va)
print(ve)
realdata = np.vstack([realhydro, realentrop]).T
#print(realdata.dot(ve).shape)
tfdata = realdata.dot(ve)
invdata = realdata.dot(ve.T)
#ax.plot(invdata[:,0], invdata[:,1], marker='.', lw=0, markersize=0.1)
#ax.plot(tfdata[:,0], tfdata[:,1], marker='.', lw=0, markersize=0.1)
#ax.plot(realdata[:,0], realdata[:,1], marker='.', lw=0, markersize=0.1)
#ax.set_aspect(1)

def hist(data, letters, chars='ACDEFGHIKLMNPQRSTVWY', split=50):
	selected = []
	for pair, r in zip(data, letters):
		if r in chars: selected.append(pair)
	selected = np.array(selected)
	bins = ax.hist(selected[:,1], bins=split, alpha=0.5)
	
	mean = np.mean(selected[:,1])
	for x, box in zip(bins[1], bins[2]):
		if mean < x: 
			ax.annotate(chars + '\nv', xy=[x, box.get_height()], ha='center')
			print(chars, x)
			#ax.annotate(chars + '\nv', xy=[x, 0], ha='center')
			#print(lastbox.xy, lastbox.height)
			#print(box.xy, box.height)
			break
		#lastbox = box

#hist(realdata, realletters)
#hist(tfdata, realletters)
dataset = tfdata
ax.set_title('Tf data')
for resn in 'ACDEFGHIKLMNPQRSTVWY':
	hist(dataset, realletters, resn)
print('(all)', np.mean(dataset[:,1]))
##hist(tfdata, realletters, 'PGS')
#hist(realdata, realletters, 'KR')
#hist(realdata, realletters, 'FLIMV')

plt.show()
