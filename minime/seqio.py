from Bio import SeqIO
import numpy as np

class Hydropathy(object):
	@staticmethod
	def parse(f=None, seq=None):
		selfobj = Hydropathy()

		if f: 
			seqio = SeqIO.parse(f, 'fasta')
			for seq in seqio:
				selfobj.seq = seq.seq
				selfobj.name = seq.name
		else: 
			if seq.startswith('>'):
				seq = Seq(seq)
				selfobj.seq = seq.seq
				selfobj.name = seq.name
			else:
				seq = Seq(seq)
				selfobj.seq = seq.seq
				selfobj.name = hash(self.seq)
				
