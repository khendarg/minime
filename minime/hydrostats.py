import numpy as np
import warnings

class Scale(object):
	kyte_dolittle = {
		'A': 1.8,
		'C': 2.5,
		'D': -3.5,
		'E': -3.5,
		'F': 2.8,
		'G': -0.4,
		'H': -3.2,
		'I': 4.5,
		'K': -3.9,
		'L': 3.8,
		'M': 1.9,
		'N': -3.5,
		'P': -1.6,
		'Q': -3.5,
		'R': -4.5,
		'S': -0.8,
		'T': -0.7,
		'V': 4.2,
		'W': -0.9,
		'Y': -1.3,
	}
	white_wimley = {
		'A': -0.33,
		'C': -0.22,
		'D': -2.41,
		'E': -1.61,
		'F': 0.58,
		'G': -1.14,
		'H': -1.37,
		'I': 0.81,
		'K': -1.81,
		'L': 0.69,
		'M': 0.44,
		'N': -0.43,
		'P': 0.31,
		'Q': -0.19,
		'R': -1.0,
		'S': -0.33,
		'T': -0.11,
		'V': 0.53,
		'W': 0.24,
		'Y': -0.23,
	}
	cc_hydropathy = {
		'A': 0.495,
		'C': -0.081,
		'D': -9.573,
		'E': -3.173,
		'F': 0.370,
		'G': -0.386,
		'H': -2.029,
		'I': 0.528,
		'K': -2.101,
		'L': 0.342,
		'M': 0.324,
		'N': -2.354,
		'P': 0.322,
		'Q': -2.176,
		'R': -4.383,
		'S': -0.936,
		'T': -0.853,
		'V': 0.308,
		'W': 0.270,
		'Y': -1.677,
	}
	@staticmethod
	def get_scale(name='kyte-dolittle', invert=False, noextrapolate=False):
		'''Retrieves a number of useful hydropathy scales

    Parameters
    ----------

    name : {'kyte-dolittle', 'white-wimley', 'contact'}
      Name of scale to fetch. Defaults to Kyte-Dolittle. For a brief overview
      of the available scales, see https://en.wikipedia.org/Hydropathy_index

    invert : bool, optional
      Saier Lab convention renders hydrophobic residues above 0 and hydrophilic
      residues below 0. Setting this parameter inverts the scale.

    noextrapolate : bool, optional
      Saier Lab convention extrapolates hydropathy values for 
      unlisted/ambiguous residues (B=Asx, J=Xle, U=Sec, Z=Glx) from known
      hydropathy values. Setting this eliminates these non-empirical values.

    Returns
    -------

    out : dict
      A dictionary containing hydropathy values.

		'''
		scale = None
		if 'kyte' in name.lower() or 'dolittle' in name.lower(): 
			scale = Scale.kyte_dolittle
		elif 'white' in name.lower() or 'wimley' in name.lower(): 
			scale = Scale.white_wimley
		elif 'contact' in name.lower() or 'cc' in name.lower():
			scale = Scale.cc_hydropathy
		else: raise KeyError('Unknown hydropathy scale: {}'.format(name))

		if not noextrapolate: 
			#These are technically interpolations, but the parameter is fairly unlikely to ever be used anyway
			if 'B' not in scale: scale['B'] = (scale['D'] + scale['N']) / 2
			if 'J' not in scale: scale['J'] = (scale['I'] + scale['L']) / 2
			if 'Z' not in scale: scale['Z'] = (scale['E'] + scale['Q']) / 2
			#This is likely the least accurate value:
			if 'U' not in scale: scale['U'] = (2 * scale['C'] - scale['S'])

		if invert: return {k: -scale[k] for k in scale}
		else: return scale

class Hydropathy(object):
	def __init__(self, seq, tessellate=False, window=19, scale='kyte-dolittle', notfound=np.nan):
		''' Provides a unified interface for handling hydropathy data. Can be recycled for
    parameters 

    Parameters
    ----------

    seq : str
      An iterable containing one letter codes for aminoacyl residues. 

    tessellate : bool, optional
      Wrap the sequence instead of treating the termini as termini. This may be
      useful for signal-processing approaches but does not yield physically 
      relevant values within a half-window-width from the termini.

    window : int, optional
      Size of window for averaging hydropathies. Defaults to 19, suitable for 
      most TMHs. Lower window sizes will tend to improve sensitivity to small 
      TMHs/small inter-TMH loops at the expense of increasing noise.

    scale : {str, dict}
      Hydropathy scale to use. String arguments (of which the default is one) 
      are resolved by the Scale helper class.

    notfound : {nan, float, Exception}
      Value for non-whitespace residues found in sequences (typically O, X, -, 
      or *). Defaults to np.nan but accepts floats as well. Supplying a
      subclass of BaseException results in said exception being raised for
      residues not found in whichever scale was requested.

    Returns
    -------

    np.ndarray : A NumPy array containing hydropathy values.

    See also
    --------

    Scale : Helper class for dealing with hydropathy scales.
        '''


		self.seq = ''.join([r for r in seq if r.split()])
		self.tessellate = tessellate
		self.window = window
		if type(scale) is str: self.scale = Scale.get_scale(scale)
		elif type(scale) is dict: self.scale = scale
		self.notfound = notfound

		self.hydropathy = np.array([])
		self.calc_hydropathy()
		#return self.get_hydropathy()

	def get_hydropathy(self):
		''' Computes hydropathy '''
		self.calc_hydropathy()
		return self.hydropathy

	def calc_hydropathy(self):
		if not self.seq: 
			self.hydropathy = np.array([])
			return
		if self.window == 1:
			hydropathy = []
			for n, r in enumerate(self.seq):
				v = self.scale.get(r, self.notfound)
				if issubclass(type(v), BaseException): raise v('Residue "{}" at position {} not found in scale'.format(r, n))
				hydropathy.append(v)
			self.hydropathy = np.array(hydropathy)

		else:
			hydropathy = []
			rawhydropathy = []

			#gauss = lambda x: np.cos(x)
			#termsigma = np.pi/2
			gauss = lambda x: np.exp(-x**2 / 2)
			termsigma = 2.0
			t = np.arange(0, self.window) - self.window//2
			scale = termsigma / ((max(t)-min(t))/2)
			kernel = gauss(t*scale)
			kernel /= sum(kernel)
			#kernel = np.ones(self.window)/self.window

			rawhydropathy = [self.scale.get(r, np.nan) for r in self.seq]
			hydropathy = np.convolve(rawhydropathy, kernel, 'same')
			for i in range(0, self.window//2):
				hydropathy[i] = np.nan
				hydropathy[-i-1] = np.nan
			#for n, r in enumerate(self.seq):
			#	v = self.scale.get(r, self.notfound)
			#	if issubclass(type(v), BaseException): raise v('Residue "{}" at position {} not found in scale'.format(r, n))
			#	rawhydropathy.append(v)
			#
			#if self.tessellate:
			#	for i in range(len(rawhydropathy)):
			#		hydropathy.append(0)
			#		for j in range(-self.window//2, self.window - self.window//2):
			#			hydropathy[i] += rawhydropathy[(i+j)%len(rawhydropathy)]
			#		hydropathy[i] /= self.window
			#else:
			#	#first, compute the raw hydropathies
			#	for i in range(self.window//2): hydropathy.append(np.nan)
			#	for i in range(0, len(rawhydropathy)-self.window):
			#		hydropathy.append(np.mean(rawhydropathy[i:i+self.window]))
			#	for i in range(self.window - self.window//2): hydropathy.append(np.nan)
			self.hydropathy = np.array(hydropathy)
		return self.hydropathy
	def __len__(self): return len(self.hydropathy)
	def __iter__(self): return iter(self.hydropathy)

	def correlation(self, other):
		''' Returns the correlation between two hydropathies (or a hydropathy and 
    something else)

    Parameters
    ----------

    other : {Hydropathy, iterable}
      What to compare against. Accepts other Hydropathy objects or iterables of
      floats.

    Returns
    -------

    out : np.ndarray, int
      Returns the unsquared Pearson's correlation coefficient for the inputs and
      the size of the overlap.

    See also
    --------

    minime.psistats.Correlation
        '''
		#if len(self) != len(other): warnings.warn('Arguments have different lengths')

		data1 = self.hydropathy
		if type(other) is Hydropathy:
			data2 = other.hydropathy
		else: data2 = iter(other)

		nans = 0
		hydro1 = []
		hydro2 = []
		#BAD HACK FIXME
		for h1, h2 in zip(data1, data2):
			if np.isnan(h1) and np.isnan(h2): continue
			elif np.isnan(h1) or np.isnan(h2): 
				nans += 1
				continue
			else:
				hydro1.append(h1)
				hydro2.append(h2)
		if len(hydro1) < 2: return np.nan, len(hydro1)
		else: return np.corrcoef(hydro1, hydro2)[1,0], len(hydro1)
