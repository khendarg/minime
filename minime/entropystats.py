import numpy as np

class Conformational(object):
	#PMC2323849 Table 3
	df = {
		'A': 2,
		'B': 4,
		'C': 4,
		'D': 4,
		'E': 5,
		'F': 4,
		'G': 3,
		'H': 4,
		'I': 4,
		'J': 4,
		'K': 6,
		'L': 4,
		'M': 5,
		'N': 4,
		'P': 1,
		'Q': 5,
		'R': 6,
		'S': 4,
		'T': 4,
		'U': 4,
		'V': 3,
		'W': 4,
		'Y': 5,
		'Z': 5,
	}
	def __init__(self, seq, tessellate=False, window=9):
		self.seq = ''.join([r for r in seq if r.strip()])
		self.tessellate = tessellate
		self.window = window

		self.entropy = np.array([])

		self.calc_entropy()

	def __len__(self): return len(self.entropy)
	def __iter__(self): return iter(self.entropy)

	def get_entropy(self):
		self.calc_entropy()
		return self.entropy

	def calc_entropy(self):
		if not self.seq: 
			self.entropy = np.array([])
			return
		entropy = []
		if self.window == 1:
			entropy = [self.df[r] for r in self.seq]
		else:

			gauss = lambda x: np.exp(-x**2 / 2)
			termsigma = 2.0
			t = np.arange(0, self.window) - self.window//2
			scale = termsigma / ((max(t)-min(t))/2)
			kernel = gauss(t*scale)
			kernel /= sum(kernel)
			 

			rawentropy = [self.df.get(r, np.nan) for r in self.seq]
			entropy = np.convolve(rawentropy, kernel, 'same')
			#entropy = entropy[self.window//2:len(entropy)-self.window+self.window//2]
			for i in range(0, self.window//2):
				entropy[i] = np.nan
				entropy[-i-1] = np.nan
			#for i, r in enumerate(rawentropy):
				#entropy.append(0)
				
				#if i < self.window//2: entropy[i] = np.nan
				#elif i > len(rawentropy) - self.window + self.window//2: entropy[i] = np.nan

				#if self.tessellate:
				#	for j in (-self.window//2, self.window-self.window//2):
				#		entropy[i] += rawentropy[(i+j) % len(rawentropy)]
				#else:
				#	if i < self.window//2: entropy[-1] = np.nan
				#	elif i > len(rawentropy) - self.window + self.window//2: entropy[-1] = np.nan
				#	else:
				#		for j in (-self.window//2, -self.window//2 + self.window):
				#			entropy[i] += rawentropy[i+j]
				#		#entropy[i] /= 
				#	#for j in (-self.window//2, self.window-self.window//2):
				#if ~np.isnan(entropy[-1]): entropy[-1] /= self.window
		self.entropy = np.array(entropy)
		#self.entropy = entropy - np.nanmean(entropy)

class Shannon(object):
	def __init__(self, seq, tessellate=False, window=9, subwindow=19):
		self.seq = ''.join([r for r in seq if r.strip()])
		self.window = window
		self.subwindow = subwindow

		self.entropy = np.array([])

		self.calc_entropy()

	def __len__(self): return len(self.entropy)
	def __iter__(self): return iter(self.entropy)

	def get_entropy(self):
		self.calc_entropy()
		return self.entropy

	def calc_entropy(self):
		if not self.seq:
			self.entropy = np.array([])
			return
		entropy = []

		if self.subwindow == 1:
			entropy = [0.] * len(self.seq)
		else:
			gauss = lambda x: np.exp(-x**2 / 2)
			termsigma = 2.0
			t = np.arange(0, self.window) - self.window//2
			scale = termsigma / ((max(t) - min(t))/2)
			kernel = gauss(t * scale)
			kernel /= sum(kernel)

			rawentropy = []
			#for i in range(0, self.subwindow//2): rawentropy.append(999)
			for i in range(0, len(self.seq) - self.subwindow):
				pos = {}
				for j in range(0, self.subwindow):
					try: pos[self.seq[i+j]] += 1
					except KeyError: pos[self.seq[i+j]] = 1
				count = np.array(list(pos.values()))
				p = count/sum(count)
				e = sum(-p * np.log2(p))
				rawentropy.append(e)
			#for i in range(0, self.subwindow - self.subwindow//2): rawentropy.append(999)
			entropy = np.convolve(rawentropy, kernel, 'same')
			for i in range(0, self.window//2):
				entropy = np.hstack([[np.nan], entropy])
				entropy = np.hstack([entropy, [np.nan]])
			while len(entropy) < len(self.seq): 
				entropy = np.hstack([entropy, [np.nan]])
		self.entropy = np.array(entropy)
