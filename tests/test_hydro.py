#!/usr/bin/env python3
import unittest
from minime import hydrostats
from Bio import SeqIO
import os
import numpy as np

class TestHydropathySingle(unittest.TestCase):
	seq1 = SeqIO.read('sequences/family-2.A.1.1.1.faa', 'fasta')
	seq2 = SeqIO.read('sequences/family-2.A.1.1.2.faa', 'fasta')
	seq3 = SeqIO.read('sequences/family-2.A.1.2.1.faa', 'fasta')
	seq4 = SeqIO.read('sequences/family-2.A.2.1.1.faa', 'fasta')
	seq5 = SeqIO.read('sequences/family-2.A.100.1.1.faa', 'fasta')

	def test_window1(self):
		hydro = hydrostats.Hydropathy(self.seq1, window=1)
		values = hydro.get_hydropathy()
		assert len(self.seq1) == len(values)

	def test_window19(self):
		window = 19
		hydro = hydrostats.Hydropathy(self.seq1, window=window)
		values = hydro.get_hydropathy()
		#assert (len(self.seq1) - window) == sum(~np.isnan(values))

	def test_tesselate19(self):
		window = 19
		hydro = hydrostats.Hydropathy(self.seq1, window=window, tessellate=True)
		values = hydro.get_hydropathy()
		#assert len(self.seq1) == sum(~np.isnan(values))

	def test_otherscale(self):
		hydro = hydrostats.Hydropathy(self.seq1, scale='white-wimbley')
		hydro = hydrostats.Hydropathy(self.seq1, scale='cc')

class TestHydropathyAlignment(unittest.TestCase):
	aln1 = list(SeqIO.parse('fragments/2.A.1.1.1.faa', 'fasta'))
	aln2 = list(SeqIO.parse('fragments/2.A.1.1.2.faa', 'fasta'))
	aln3 = list(SeqIO.parse('fragments/2.A.1.2.1.faa', 'fasta'))
	aln4 = list(SeqIO.parse('fragments/2.A.2.1.1.faa', 'fasta'))
	aln5 = list(SeqIO.parse('fragments/2.A.100.1.1.faa', 'fasta'))
	aln6 = list(SeqIO.parse('fragments/5.B.2.1.1.faa', 'fasta'))
	aln9 = list(SeqIO.parse('fragments/3.A.1.108.1.faa', 'fasta'))

	def test_identity(self):
		hydro = [hydrostats.Hydropathy(seq) for seq in self.aln1]
		R, N = hydro[0].correlation(hydro[1]) 
		#print(1, R, N)
		assert 0.99 <= R <= 1.0
		assert 400 <= N

	def test_samesubfam(self):
		hydro = [hydrostats.Hydropathy(seq) for seq in self.aln2]
		R, N = hydro[0].correlation(hydro[1])
		#print(2, R, N)
		#R = 0.58
		#assert 0.5 <= R <= 1.0
		assert 400 <= N

	def test_samefam(self):
		hydro = [hydrostats.Hydropathy(seq) for seq in self.aln3]
		R, N = hydro[0].correlation(hydro[1])
		#print(3, R, N)
		#R = 0.80
		#assert 0.7 <= R <= 0.8
		assert 100 <= N

	def test_samesuperfam(self):
		hydro = [hydrostats.Hydropathy(seq) for seq in self.aln4]
		R, N = hydro[0].correlation(hydro[1])
		#print(4, R, N)
		#R = 0.62
		#assert 0.6 <= R <= 0.7
		assert 60 <= N <= 80

	def test_probable(self):
		hydro = [hydrostats.Hydropathy(seq) for seq in self.aln5]
		R, N = hydro[0].correlation(hydro[1])
		#print(5, R, N)
		#R = 0.30
		#assert 0.1 <= R <= 0.4
		assert N <= 50

	def test_possible(self):
		hydro = [hydrostats.Hydropathy(seq) for seq in self.aln6]
		R, N = hydro[0].correlation(hydro[1])
		#print(6, R, N)
		#R = 0.04
		#assert 0 <= R <= 0.1
		assert N <= 50

	def test_negativecontrol(self):
		hydro = [hydrostats.Hydropathy(seq) for seq in self.aln9]
		R, N = hydro[0].correlation(hydro[1])
		#print(9, R, N)
		assert np.isnan(R)
		assert N == 0

if __name__ == '__main__':
	unittest.main()
