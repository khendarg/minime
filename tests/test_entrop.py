#!/usr/bin/env python3
import unittest
from minime import entropystats
from Bio import SeqIO
import os
import numpy as np

class TestEntropySingle(unittest.TestCase):
	seq1 = SeqIO.read('sequences/family-2.A.1.1.1.faa', 'fasta')
	seq2 = SeqIO.read('sequences/family-2.A.1.1.2.faa', 'fasta')
	seq3 = SeqIO.read('sequences/family-2.A.1.2.1.faa', 'fasta')
	seq4 = SeqIO.read('sequences/family-2.A.2.1.1.faa', 'fasta')
	seq5 = SeqIO.read('sequences/family-2.A.100.1.1.faa', 'fasta')
	seq9 = SeqIO.read('sequences/family-3.A.1.108.1.faa', 'fasta')
	seq0 = SeqIO.read('sequences/family-3.A.1.106.2.faa', 'fasta')
	seqano = SeqIO.read('sequences/family-1.A.17.5.10.faa', 'fasta')

	def test_window1(self):
		entrop = entropystats.Conformational(self.seq1, window=1)
		values = entrop.get_entropy()
		assert len(self.seq1) == len(values)

	def test_window9(self):
		entrop = entropystats.Conformational(self.seqano, window=9)
		values = entrop.get_entropy()


		#import matplotlib.pyplot as plt
		#plt.plot(values)

		#from minime import hydrostats
		#hydro = hydrostats.Hydropathy(self.seqano, window=19)
		#values = hydro.get_hydropathy()
		#plt.plot(values)
		#print(hydro.correlation(entrop))

		##plt.plot(hydro.hydropathy, entrop.entropy, lw=0.1)
		##plt.scatter(hydro.hydropathy, entrop.entropy, marker='.', lw=0, c=np.arange(0, 1, 1/len(entrop.entropy)), cmap='viridis')
		##plt.ylim([1, 7])
		##plt.xlim([-3, 3])
		#plt.show()
		##assert len(self.seq1) == len(values) + 19

	def test_shannon(self):
		entrop = entropystats.Shannon(self.seq1, window=19, subwindow=19)

if __name__ == '__main__':
	unittest.main()
